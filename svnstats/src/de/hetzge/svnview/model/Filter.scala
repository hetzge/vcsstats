package de.hetzge.svnview.model

import java.time._

final case class Filter(
    from: Instant = Instant.MIN,
    to: Instant = Instant.MAX,
    includeUsers: Set[String] = Set(),
    excludeUsers: Set[String] = Set(),
    includeCommits: Set[String] = Set(),
    excludeCommits: Set[String] = Set(),
    includeFiles: Set[String] = Set(),
    excludeFiles: Set[String] = Set()
) {
  def key: String = s"""${from.getEpochSecond}_${to.getEpochSecond}_${includeUsers.mkString(",")}_${excludeUsers.mkString(",")}_${includeCommits.mkString(",")}_${excludeCommits.mkString(",")}_${includeFiles.mkString(",")}_${excludeFiles.mkString(",")}"""
}
