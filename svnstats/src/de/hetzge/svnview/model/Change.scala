package de.hetzge.svnview.model

import java.time._
import com.zutubi.diff._

final case class Change(
    oldFile: String,
    newFile: String,
    hunks: Seq[Hunk],
    patchType: PatchType,
    author: String,
    instant: Instant,
    commitKey: String,
    message: String
) {
  def addedLineCount   = hunks.map(_.addedLineCount).foldLeft(0)(_ + _)
  def deletedLineCount = hunks.map(_.deletedLineCount).foldLeft(0)(_ + _)
}
