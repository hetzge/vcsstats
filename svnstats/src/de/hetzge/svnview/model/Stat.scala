package de.hetzge.svnview.model

import java.time._

final case class StatGroup(context: StatContext = StatContext(), stat: Stat = Stat()) {
  def +(other: StatGroup): StatGroup = {
    StatGroup(context = context + other.context, stat = stat + other.stat)
  }
}

final case class StatContext(files: Set[String] = Set(), users: Set[String] = Set(), commits: Set[Commit] = Set()) {
  def +(other: StatContext): StatContext = {
    StatContext(files = files ++ other.files, users = users ++ other.users, commits = commits ++ other.commits)
  }
}

final case class Stat(linesAdded: Int = 0, linesDeleted: Int = 0) {
  def +(other: Stat): Stat = {
    Stat(linesAdded = linesAdded + other.linesAdded, linesDeleted = linesDeleted + other.linesDeleted)
  }
}

sealed class StatGroupType(val key: String)
final case object UserStatGroupType   extends StatGroupType("USER")
final case object FileStatGroupType   extends StatGroupType("FILE")
final case object CommitStatGroupType extends StatGroupType("COMMIT")
final case object MonthStatGroupType  extends StatGroupType("MONTH")
final case object MixedStatGroupType  extends StatGroupType("MIXED")
