package de.hetzge.svnview.model.ui

import io.circe._
import java.time._
import de.hetzge.svnview.model._

final case class StatTable(columns: List[String], rows: List[StatTableRow]) {
  def sumRow: StatTableRow = rows.reduce(_ + _)
  def sort(columnIndex: Int): StatTable = {
    if (rows.isEmpty) { return this }

    val valueIndex = Math.ceil(columnIndex / 2f).toInt - 1
    copy(rows = rows.sortWith((a, b) => {
      if (columnIndex == 0) {
        a.label < b.label
      } else if (columnIndex - 1 == columns.size * 2) {
        a.sum.linesAdded > b.sum.linesAdded
      } else if (columnIndex - 1 == columns.size * 2 + 1) {
        a.sum.linesDeleted > b.sum.linesDeleted
      } else if (columnIndex % 2 != 0) {
        a.values(valueIndex).linesAdded > b
          .values(valueIndex)
          .linesAdded
      } else if (columnIndex % 2 == 0) {
        a.values(valueIndex).linesDeleted > b
          .values(valueIndex)
          .linesDeleted
      } else {
        throw new IllegalStateException
      }
    }))
  }

  def json: Json = {
    Json.obj(
      ("header", headerJson),
      ("rows", Json.arr(rows.map(_.json): _*)),
      ("footer", sumRow.json),
      ("addedChart", addedChartJson),
      ("removedChart", removedChartJson)
    )
  }

  def addedChartJson: Json = {
    Json.obj(
      ("labels", Json.arr(columns.map(Json.fromString(_)): _*)),
      ("series", Json.arr(rows.map(_.addedChartSeriesJson): _*))
    )
  }

  def removedChartJson: Json = {
    Json.obj(
      ("labels", Json.arr(columns.map(Json.fromString(_)): _*)),
      ("series", Json.arr(rows.map(_.removedChartSeriesJson): _*))
    )
  }

  private def headerJson: Json = {
    val labelColumn = List(Json.fromString("Objects"))
    val valueColumns = columns
      .flatMap(name => List(s"+$name", s"-$name"))
      .map(Json.fromString(_))
    val sumColumns = List(Json.fromString("+sum"), Json.fromString("-sum"))
    Json.arr(labelColumn ++ valueColumns ++ sumColumns: _*)
  }
}