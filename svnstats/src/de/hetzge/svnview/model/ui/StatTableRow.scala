package de.hetzge.svnview.model.ui

import io.circe._
import de.hetzge.svnview.model._

final case class StatTableRow(label: String, values: List[Stat]) {
  def sum: Stat = values.reduce(_ + _)
  def +(other: StatTableRow): StatTableRow = {
    StatTableRow(
      label = "sum",
      values = values.zip(other.values).map {
        case (a, b) => a + b
      }
    )
  }

  def json: Json = {
    val labelColumn = List(Json.fromString(label))
    val valueColumns = values
      .flatMap(stat => List(stat.linesAdded, -stat.linesDeleted))
      .map(Json.fromInt(_))
    val sumColumn =
      List(Json.fromInt(sum.linesAdded), Json.fromInt(-sum.linesDeleted))
    Json.arr(labelColumn ++ valueColumns ++ sumColumn: _*)
  }

  def addedChartSeriesJson = {
    Json.obj(
      ("name", Json.fromString(label)),
      ("data", Json.arr(values.map(_.linesAdded).map(Json.fromInt): _*))
    )
  }

  def removedChartSeriesJson = {
    Json.obj(
      ("name", Json.fromString(label)),
      ("data", Json.arr(values.map(-_.linesDeleted).map(Json.fromInt): _*))
    )
  }
}