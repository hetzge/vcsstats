package de.hetzge.svnview.model.ui

import io.circe._
import java.time._
import java.time.format._
import de.hetzge.svnview.model._

final case class StatTreeRow(
    name: String = "???",
    groupType: StatGroupType = MixedStatGroupType,
    statGroup: StatGroup = StatGroup(),
    filter: Filter = Filter()
) {

  def prepareChildren(): List[StatTreeRow] = {

    val dateTimeFormatter = DateTimeFormatter
      .ofLocalizedDateTime(FormatStyle.MEDIUM)
      .withZone(ZoneId.systemDefault())

    val fileFilters =
      statGroup.context.files.toList
        .filter(file => !filter.includeFiles.contains(file))
        .map(file => StatTreeRow(file, FileStatGroupType, StatGroup(), filter.copy(includeFiles = Set(file))))

    val userFilters =
      statGroup.context.users.toList
        .filter(user => !filter.includeUsers.contains(user))
        .map(user => StatTreeRow(user, UserStatGroupType, StatGroup(), filter.copy(includeUsers = Set(user))))

    val commitFilters =
      statGroup.context.commits.toList
        .filter {
          case Commit(key) => !filter.includeCommits.contains(key)
        }
        .map { commit =>
          StatTreeRow(s"""${dateTimeFormatter.format(commit.instant)} [${commit.key}] "${commit.message}"""", CommitStatGroupType, StatGroup(), filter.copy(includeCommits = Set(commit.key)))
        }

    fileFilters ++ userFilters ++ commitFilters
  }

  def json: Json = {
    Json.obj(
      ("name", Json.fromString(name)),
      ("type", Json.fromString(groupType.key)),
      ("linesAdded", Json.fromInt(statGroup.stat.linesAdded)),
      ("linesRemoved", Json.fromInt(-statGroup.stat.linesDeleted)),
      ("userCount", Json.fromInt(statGroup.context.users.size)),
      ("commitCount", Json.fromInt(statGroup.context.commits.size)),
      ("fileCount", Json.fromInt(statGroup.context.files.size))
    )
  }
}