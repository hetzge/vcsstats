package de.hetzge.svnview.model

import java.time._

final case class Commit(key: String)(val instant: Instant, val message: String) 