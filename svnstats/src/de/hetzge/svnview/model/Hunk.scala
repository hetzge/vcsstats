package de.hetzge.svnview.model

import scala.collection.JavaConverters._

import com.zutubi.diff._
import com.zutubi.diff.unified._
import com.zutubi.diff.unified.UnifiedHunk._

final case class HunkLine(content: String, lineType: LineType)

object Hunk {
  def apply(unifiedHunk: UnifiedHunk): Hunk = {
    Hunk(
      oldLineNumber = unifiedHunk.getOldOffset,
      newLineNumber = unifiedHunk.getNewOffset,
      lines = unifiedHunk.getLines.asScala.map(line => HunkLine(line.getContent, line.getType)),
      debugString = unifiedHunk.toString()
    )
  }
}
final case class Hunk(oldLineNumber: Long, newLineNumber: Long, lines: Seq[HunkLine], debugString: String = "") {
  def addedLineCount: Int   = lines.count(_.lineType == LineType.ADDED)
  def deletedLineCount: Int = lines.count(_.lineType == LineType.DELETED)
  def addedLineRange        = newLineNumber until (newLineNumber + addedLineCount)
  def deletedLineRange      = oldLineNumber until (oldLineNumber + deletedLineCount)
  def lineCountDifference   = addedLineCount - deletedLineCount
  def newLinesByLineNumber: Seq[(Long, HunkLine)] = {
    addedLineRange
      .zip(lines.filter(_.lineType != LineType.DELETED))
      .filter(_._2.lineType != LineType.COMMON)
  }
  def oldLinesByLineNumber: Seq[(Long, HunkLine)] = {
    deletedLineRange
      .zip(lines.filter(_.lineType != LineType.ADDED))
      .filter(_._2.lineType != LineType.COMMON)
  }
  def linesByLineNumber = newLinesByLineNumber ++ oldLinesByLineNumber
}
