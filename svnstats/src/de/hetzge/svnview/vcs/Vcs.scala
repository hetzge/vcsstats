package de.hetzge.svnview.vcs

import de.hetzge.svnview.model._

import monix.reactive.Observable

trait Vcs {
  def lastCommitKey: String
  def patches(fromCommitKeyOption: Option[String]): Observable[Change]
}
