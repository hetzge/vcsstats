package de.hetzge.svnview.vcs

import java.io.ByteArrayOutputStream
import java.io.File
import java.io.StringReader

import org.apache.commons.io.FileUtils
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.diff.DiffFormatter
import org.eclipse.jgit.lib.ObjectId
import org.eclipse.jgit.lib.ObjectReader
import org.eclipse.jgit.lib.Repository
import org.eclipse.jgit.revwalk.RevSort
import org.eclipse.jgit.revwalk.RevWalk
import org.eclipse.jgit.treewalk.AbstractTreeIterator
import org.eclipse.jgit.treewalk.CanonicalTreeParser
import org.eclipse.jgit.treewalk.EmptyTreeIterator

import com.zutubi.diff.PatchFileParser
import com.zutubi.diff.git.GitPatchParser
import com.zutubi.diff.git.GitUnifiedPatch

object GitUtils extends App {
  def blame(repository: Repository, filePath: String) = {
    val blameResult = new Git(repository).blame().setFilePath(filePath).call()
    val rawText     = blameResult.getResultContents
    for (i <- 0 until rawText.size()) {
      // author = user who wrote the line
      // commiter = user who commited the line

      val author      = blameResult.getSourceAuthor(i)
      val commiter    = blameResult.getSourceCommitter(i)
      val commit      = blameResult.getSourceCommit(i)
      val sourceLine  = blameResult.getSourceLine(i)
      val lineContent = rawText.getString(i)
    }
  }
}
