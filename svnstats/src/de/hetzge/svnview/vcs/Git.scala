package de.hetzge.svnview.vcs

import de.hetzge.svnview.model._

import org.eclipse.jgit.api.Git
import org.eclipse.jgit.diff.DiffFormatter
import org.eclipse.jgit.lib.ObjectId
import org.eclipse.jgit.lib.ObjectReader
import org.eclipse.jgit.lib.Repository
import org.eclipse.jgit.revwalk.RevSort
import org.eclipse.jgit.revwalk.RevWalk
import org.eclipse.jgit.revwalk.RevCommit
import org.eclipse.jgit.treewalk.AbstractTreeIterator
import org.eclipse.jgit.treewalk.CanonicalTreeParser
import org.eclipse.jgit.treewalk.EmptyTreeIterator
import org.eclipse.jgit.lib.Constants

import com.zutubi.diff.PatchFileParser
import com.zutubi.diff.Patch
import com.zutubi.diff.git.GitPatchParser
import com.zutubi.diff.git.GitUnifiedPatch
import com.zutubi.diff.PatchParseException

import java.io.ByteArrayOutputStream
import java.io.File
import java.io.StringReader

import org.apache.commons.io.FileUtils
import scala.collection.JavaConverters._
import monix.eval.Task
import monix.reactive.Observable
import java.time.Instant

final case class GitConfig(val uri: String, val branch: String = "master")

final class GitVcs(config: GitConfig) extends Vcs {

  val folder = new File("/home/hetzge/temp/repo")
  FileUtils.deleteDirectory(folder)

  val git = Git
    .cloneRepository()
    .setURI(config.uri)
    .setDirectory(folder)
    .setBranch(config.branch)
    .call()

  val repository = git.getRepository

  override def lastCommitKey = {
    val headObjectId = repository.resolve(Constants.HEAD);

    git
      .log()
      .add(headObjectId)
      .setMaxCount(1)
      .call()
      .iterator
      .next
      .getId
      .getName
  }

  override def patches(fromCommitKeyOption: Option[String] = None) = {

    var walk: RevWalk = null
    try {
      walk = new RevWalk(repository)
      walk.sort(RevSort.COMMIT_TIME_DESC, true)
      walk.sort(RevSort.REVERSE, true)

      fromCommitKeyOption match {
        case Some(key) =>
          walk.markStart(walk.parseCommit(repository.resolve(key)))
        case None =>
          walk.markStart(walk.parseCommit(repository.resolve("origin/master")))
      }

      // skip because "from" is exclusive
      val skip = fromCommitKeyOption match {
        case Some(key) => 1
        case None      => 0
      }

      var counter                = 0
      var lastCommitName: String = null

      Observable
        .fromIterable(walk.asScala)
        .drop(skip)
        .flatMap { commit =>
          val commitName    = commit.getId.getName
          val newTreeParser = prepareTreeParser(repository, commitName)
          val oldTreeParser =
            if (lastCommitName != null)
              prepareTreeParser(repository, lastCommitName)
            else new EmptyTreeIterator
          lastCommitName = commitName

          Observable
            .fromIterable(
              git.diff
                .setNewTree(newTreeParser)
                .setOldTree(oldTreeParser)
                .call()
                .asScala)
            .flatMap { diffEntry =>
              var diffFormater: DiffFormatter = null
              try {

                val out = new ByteArrayOutputStream()
                diffFormater = new DiffFormatter(out)
                diffFormater.setRepository(repository)
                diffFormater.format(diffEntry)
                val diff = new String(out.toByteArray(), "UTF-8")

                try {
                  val parser = new PatchFileParser(new GitPatchParser)
                  val parsed = parser.parse(new StringReader(diff))

                  counter += 1
                  if (counter % 10000 == 0) {
                    println(counter)
                  }

                  Observable
                    .fromIterable(parsed.getPatches.asScala)
                    .map(_.asInstanceOf[GitUnifiedPatch])
                    .map(createChange(commit, _))
                } catch {
                  case e: PatchParseException => {
                    println("ERROR ............")
                    println(diff)
                    e.printStackTrace()
                    Observable.empty[Change]
                  }
                }
              } finally {
                if (diffFormater != null) {
                  diffFormater.close()
                }
              }
            }
        }
    } finally {
      if (walk != null) walk.close()
    }
  }

  private def createChange(commit: RevCommit, patch: GitUnifiedPatch): Change = {
    new Change(
      author = commit.getAuthorIdent.getName,
      instant = Instant.ofEpochSecond(commit.getCommitTime),
      commitKey = commit.getName,
      message = commit.getFullMessage,
      oldFile = patch.getOldFile,
      newFile = patch.getNewFile,
      hunks = patch.getHunks.asScala.map(Hunk(_)),
      patchType = patch.getType
    )
  }

  def prepareTreeParser(repository: Repository, objectId: String): CanonicalTreeParser = {
    var walk: RevWalk = null
    try {
      walk = new RevWalk(repository)
      val commit               = walk.parseCommit(ObjectId.fromString(objectId))
      val tree                 = walk.parseTree(commit.getTree.getId)
      val parser               = new CanonicalTreeParser
      var reader: ObjectReader = null
      try {
        reader = repository.newObjectReader
        parser.reset(reader, tree.getId)
        walk.dispose()
        return parser
      } finally {
        if (reader != null) {
          reader.close()
        }
      }
    } finally {
      if (walk != null) {
        walk.close()
      }
    }
  }
}
