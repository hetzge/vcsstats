package de.hetzge.svnview.vcs

import de.hetzge.svnview.model._

import org.tmatesoft.svn.core.SVNDepth
import org.tmatesoft.svn.core.SVNLogEntry
import org.tmatesoft.svn.core.SVNURL
import org.tmatesoft.svn.core.io.SVNRepositoryFactory
import org.tmatesoft.svn.core.wc.SVNClientManager
import org.tmatesoft.svn.core.wc.SVNRevision
import org.tmatesoft.svn.core.wc.SVNWCUtil
import org.tmatesoft.svn.core.wc2.SvnOperationFactory
import org.tmatesoft.svn.core.internal.wc2.ng.SvnDiffGenerator
import org.tmatesoft.svn.core.wc2.SvnTarget
import org.tmatesoft.svn.core.internal.io.svn.SVNRepositoryFactoryImpl
import org.tmatesoft.svn.core.io.SVNRepository
import org.tmatesoft.svn.core.auth.ISVNAuthenticationManager

import com.zutubi.diff.PatchFileParser
import com.zutubi.diff.unified.UnifiedPatch
import com.zutubi.diff.unified.UnifiedPatchParser
import org.apache.commons.io.FileUtils
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.time.Instant
import java.io.StringReader
import scala.util._
import scala.collection.JavaConverters._
import monix.eval.Task
import monix.reactive.Observable

final case class SvnConfig(val username: String = "", val password: String = "", val url: String)

final class SvnVcs(config: SvnConfig) extends Vcs {
  private val svnUrl = SVNURL.parseURIEncoded(config.url)

  override def lastCommitKey = createRepository().getLatestRevision.toString

  override def patches(fromCommitKeyOption: Option[String] = None) = {
    val startRevision = fromCommitKeyOption.map(_.toInt + 1).getOrElse(0)
    val endRevision   = -1

    println(s"startRevision: $startRevision, endRevision: $endRevision")

    log(startRevision, endRevision).flatMap(logEntry => diff(logEntry).map(patch => createChange(logEntry, patch)))
  }

  private def createChange(logEntry: SVNLogEntry, patch: UnifiedPatch): Change = {
    new Change(
      oldFile = patch.getOldFile,
      newFile = patch.getNewFile,
      hunks = patch.getHunks.asScala.map(Hunk(_)),
      patchType = patch.getType,
      author = logEntry.getAuthor,
      instant = Instant.ofEpochMilli(logEntry.getDate.getTime),
      commitKey = logEntry.getRevision.toString,
      message = logEntry.getMessage
    )
  }

  private def createRepository(): SVNRepository = {
    /*
	   * Init repository access type.
	   * See https://svnkit.com/javadoc/org/tmatesoft/svn/core/io/SVNRepository.html
	   */
    val repository = SVNRepositoryFactory.create(svnUrl)
    repository.setAuthenticationManager(createAuthenticationManager())
    repository
  }

  private def createAuthenticationManager(): ISVNAuthenticationManager = {
    SVNWCUtil.createDefaultAuthenticationManager(null, config.username, config.password.toCharArray)
  }

  private def log(startRevision: Int, endRevision: Int): Observable[SVNLogEntry] = {
    Observable
      .fromIterable(
        createRepository()
          .log(Array(""), null, startRevision, endRevision, false, false)
          .asScala)
      .map(_.asInstanceOf[SVNLogEntry])
      .filter(_.getRevision > 0)
  }

  private def diff(logEntry: SVNLogEntry): Observable[UnifiedPatch] = {
    println(s"diff: ${logEntry.getRevision} ${Thread.currentThread.getName}")

    val out       = new ByteArrayOutputStream()
    val revisionA = SVNRevision.create(logEntry.getRevision - 1)
    val revisionB = SVNRevision.create(logEntry.getRevision)

    val authenticationManager = createAuthenticationManager()
    val clientManager         = SVNClientManager.newInstance
    clientManager.setAuthenticationManager(authenticationManager)

    Try(clientManager.getDiffClient.doDiff(svnUrl, revisionA, svnUrl, revisionB, SVNDepth.INFINITY, true, out)) match {
      case Success(ignore) => {
        // replace (nonexistent) to be compatible with parser
        val diff = out.toString("UTF-8").replace("(nonexistent)", "(revision 0)")
        val parsed = new PatchFileParser(new UnifiedPatchParser).parse(new StringReader(diff))

        println(s"finished ${Thread.currentThread.getName}")

        Observable.fromIterable(parsed.getPatches.asScala).map(_.asInstanceOf[UnifiedPatch])
      }
      case Failure(e) => {
        println("svn error: " + e.getMessage)
        Observable.empty[UnifiedPatch]
      }
    }
  }
}
