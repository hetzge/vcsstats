package de.hetzge.svnview.db

import java.time.Instant

import com.zutubi.diff.unified.UnifiedHunk.LineType

import jetbrains.exodus.entitystore.Entity
import jetbrains.exodus.entitystore.EntityId
import jetbrains.exodus.entitystore.StoreTransaction

import collection.JavaConverters._
import collection.JavaConversions._

class DbCommit(commitEntity: Entity) {
  def id  = commitEntity.getId
  def key = commitEntity.getProperty("key").toString
  def timestamp =
    Instant.ofEpochSecond(commitEntity.getProperty("timestamp").asInstanceOf[Long])
  def message = commitEntity.getBlobString("message")
  def user    = new DbUser(commitEntity.getLink("user"))
}
object DbCommit {
  def create(key: String, timestamp: Instant, message: String, userId: EntityId)(implicit tx: StoreTransaction): DbCommit = {

    val commitEntity = tx.newEntity("Commit")
    commitEntity.setProperty("key", key)
    commitEntity.setProperty("timestamp", timestamp.getEpochSecond)
    commitEntity.setBlobString("message", message)

    val userEntity = tx.getEntity(userId)
    userEntity.addLink("commits", commitEntity)
    commitEntity.setLink("user", userEntity)

    new DbCommit(commitEntity)
  }
}

class DbUser(userEntity: Entity) {
  def id       = userEntity.getId
  def username = userEntity.getProperty("username").toString
}
object DbUser {
  def create(username: String)(implicit tx: StoreTransaction): DbUser = {

    val userEntity = tx.newEntity("User")
    userEntity.setProperty("username", username)
    userEntity.getId

    new DbUser(userEntity)
  }
}

class DbLine(lineEntity: Entity) {
  def id         = lineEntity.getId
  def lineType   = LineType.valueOf(lineEntity.getProperty("type").toString)
  def lineNumber = lineEntity.getProperty("number").asInstanceOf[Int]
  def content    = lineEntity.getBlobString("content")
  def author     = new DbUser(lineEntity.getLink("author"))
  def change     = new DbChange(lineEntity.getLink("change"))
}
object DbLine {
  def create(lineType: LineType, lineNumber: Int, content: String, changeId: EntityId, authorId: EntityId)(implicit tx: StoreTransaction): DbLine = {

    val lineEntity = tx.newEntity("Line")
    lineEntity.setProperty("number", lineNumber)
    lineEntity.setProperty("type", lineType.name)
    lineEntity.setBlobString("content", content)

    val authorEntity = tx.getEntity(authorId)
    lineEntity.setLink("author", authorEntity)

    val changeEntity = tx.getEntity(changeId)
    changeEntity.addLink("lines", lineEntity)
    lineEntity.setLink("change", changeEntity)

    new DbLine(lineEntity)
  }
}

class DbChange(changeEntity: Entity) {
  def id     = changeEntity.getId
  def file   = new DbFile(changeEntity.getLink("file"))
  def commit = new DbCommit(changeEntity.getLink("commit"))
  def lines  = changeEntity.getLinks("lines").map(new DbLine(_))
  def addedLinesCount(implicit tx: StoreTransaction) =
    changeEntity
      .getLinks("lines")
      .map(new DbLine(_))
      .filter(_.lineType == LineType.ADDED)
      .size
  def removedLinesCount(implicit tx: StoreTransaction) =
    changeEntity
      .getLinks("lines")
      .map(new DbLine(_))
      .filter(_.lineType == LineType.DELETED)
      .size
}
object DbChange {
  def create(fileId: EntityId, commitId: EntityId)(implicit tx: StoreTransaction): DbChange = {

    val changeEntity = tx.newEntity("Change")

    val fileEntity = tx.getEntity(fileId)
    fileEntity.addLink("changes", changeEntity)
    changeEntity.setLink("file", fileEntity)

    val commitEntity = tx.getEntity(commitId)
    commitEntity.addLink("changes", changeEntity)
    changeEntity.setLink("commit", commitEntity)

    new DbChange(changeEntity)
  }
}

class DbFile(fileEntity: Entity) {
  def id      = fileEntity.getId
  def name    = fileEntity.getProperty("name").toString
  def changes = fileEntity.getLinks("changes").map(new DbChange(_))
}
object DbFile {
  def create(name: String)(implicit tx: StoreTransaction): DbFile = {

    val fileEntity = tx.newEntity("File")
    fileEntity.setProperty("name", name)

    new DbFile(fileEntity)
  }
}
