package de.hetzge.svnview.db

import de.hetzge.svnview.model._
import de.hetzge.svnview.model.{Change => VcsChange}
import de.hetzge.svnview.model.ui._
import de.hetzge.svnview.patcher._

import java.time.Instant
import java.time.LocalDate
import java.time.LocalTime
import java.time.Period
import java.time.ZoneId

import scala.collection.immutable.HashSet

import com.zutubi.diff.unified.UnifiedHunk.LineType

import cats.syntax.traverse._
import cats.instances.future._
import cats.instances.list._
import cats.effect._

import monix.eval.Task

import jetbrains.exodus.entitystore.Entity
import jetbrains.exodus.entitystore.EntityId
import jetbrains.exodus.entitystore.EntityIterable
import jetbrains.exodus.entitystore.PersistentEntityStore
import jetbrains.exodus.entitystore.StoreTransaction
import jetbrains.exodus.entitystore.iterate.EntityIterableBase

import collection.JavaConverters._
import collection.JavaConversions._

import java.time._
import java.time.format._
import java.time.temporal._

class Api(store: PersistentEntityStore) {

  def table(): Task[StatTable] = {

    val offsetRange = (11 to 0 by -1)
    val yearMonths = offsetRange
      .map(offset => YearMonth.now().minusMonths(offset)).toList
    val columnFilters = yearMonths
      .map(yearMonth => Filter(
        from = yearMonth.atDay(1).atStartOfDay().atZone(ZoneId.systemDefault()).toInstant, 
        to = yearMonth.atEndOfMonth.atTime(23, 59, 59, 999).atZone(ZoneId.systemDefault()).toInstant))
    val columns = yearMonths.map(_.getMonth.name)

    // for debug:
    val dateTimeFormatter = DateTimeFormatter
      .ofLocalizedDateTime(FormatStyle.MEDIUM)
      .withZone(ZoneId.systemDefault())
    columnFilters.foreach { columnFilter =>
      println((columnFilter.to.getEpochSecond() - columnFilter.from.getEpochSecond()) + " --- " + dateTimeFormatter.format(columnFilter.from) + " to " + dateTimeFormatter.format(columnFilter.to))
    }

    Task
      .gather(getAllUsers.map { user =>
        val username = store.computeInReadonlyTransaction(tx => user.username)
        Task
          .gather(columnFilters
            .map(columnFilter => query(columnFilter.copy(includeUsers = Set(username)))))
          .map(statGroups => StatTableRow(username, statGroups.map(_.stat)))
      })
      .map(rows => StatTable(columns, rows))
  }

  def query(filter: Filter): Task[StatGroup] = Task {
    store.computeInReadonlyTransaction { tx =>
      findChanges(filter).foldLeft(StatGroup()) { (statGroup, change) =>
        val file   = change.file
        val commit = change.commit
        val user   = commit.user

        statGroup.copy(
          context = statGroup.context + StatContext(
            files = Set(file.name),
            users = Set(user.username),
            commits = Set(Commit(commit.key)(commit.timestamp, commit.message))
          ),
          stat = statGroup.stat + Stat(
            linesAdded = change.addedLinesCount(tx),
            linesDeleted = change.removedLinesCount(tx)
          )
        )
      }
    }
  }

  def saveChange(change: VcsChange): Unit = {
    store.executeInTransaction(implicit tx => {
      val userId = tx
        .findIdOption("User", "username", change.author)
        .getOrElse(DbUser.create(change.author).id)
      val commitId = tx
        .findIdOption("Commit", "key", change.commitKey)
        .getOrElse(DbCommit.create(change.commitKey, change.instant, change.message, userId).id)
      val fileId = tx
        .findIdOption("File", "name", change.newFile)
        .getOrElse(DbFile.create(change.newFile).id)
      val changeId = DbChange.create(fileId, commitId).id

      change.hunks.flatMap(_.linesByLineNumber).foreach {
        case (lineNumber, line) => {
          val authorId = tx
            .findIdOption("User", "username", change.author)
            .getOrElse(DbUser.create(change.author).id)
          DbLine.create(line.lineType, lineNumber.toInt, line.content, changeId, authorId)
        }
      }
    })
  }

  def print(): Unit = {
    store.computeInReadonlyTransaction { tx =>
      println("Users:")
      getAllUsers.foreach(user => println(user.username))

      println("Files:")
      getAllFiles.foreach(file => println(file.name))

      println("Commits:")
      getAllCommits.foreach(commit => println(commit.key))
    }
  }

  def getAllUsers(): List[DbUser] =
    store.computeInReadonlyTransaction(_.getAll("User").iterator.map(new DbUser(_)).toList)
  def getAllFiles(): List[DbFile] =
    store.computeInReadonlyTransaction(_.getAll("File").iterator.map(new DbFile(_)).toList)
  def getAllCommits(): List[DbCommit] =
    store.computeInExclusiveTransaction(_.getAll("Commit").iterator.map(new DbCommit(_)).toList)

  def findChanges(filter: Filter): List[DbChange] = {
    store.computeInReadonlyTransaction { tx =>
      val before = System.currentTimeMillis

      val includeUsersIterable = if (!filter.includeUsers.isEmpty) {
        filter.includeUsers
          .map(tx.find("User", "username", _))
          .fold(EntityIterableBase.EMPTY)(_.union(_))
      } else {
        tx.getAll("User")
      }
      val excludeUsersIterable = filter.excludeUsers
        .map(tx.find("User", "username", _))
        .fold(EntityIterableBase.EMPTY)(_.union(_))
      val usersIterable = includeUsersIterable.minus(excludeUsersIterable)

      val includeCommitsIterable = if (!filter.includeCommits.isEmpty) {
        filter.includeCommits
          .map(tx.find("Commit", "key", _))
          .fold(EntityIterableBase.EMPTY)(_.union(_))
      } else {
        tx.getAll("Commit")
      }
      val excludeCommitsIterable = filter.excludeCommits
        .map(tx.find("Commit", "key", _))
        .fold(EntityIterableBase.EMPTY)(_.union(_))
      val commitsByTimestampIterable = tx.find("Commit", "timestamp", filter.from.getEpochSecond, filter.to.getEpochSecond)
      val commitsByUserIterable      = tx.findLinks("Commit", usersIterable, "user")
      val commitsIterable = includeCommitsIterable
        .minus(excludeCommitsIterable)
        .intersect(commitsByTimestampIterable.intersect(commitsByUserIterable))

      val includeFilesIterable = if (!filter.includeFiles.isEmpty) {
        filter.includeFiles
          .map(tx.findStartingWith("File", "name", _))
          .fold(EntityIterableBase.EMPTY)(_.union(_))
      } else {
        tx.getAll("File")
      }
      val excludeFilesIterable = filter.excludeFiles
        .map(tx.findStartingWith("File", "name", _))
        .fold(EntityIterableBase.EMPTY)(_.union(_))
      val filesIterable = includeFilesIterable.minus(excludeFilesIterable)

      val changesByCommitsIterable =
        tx.findLinks("Change", commitsIterable, "commit")
      val changesByFileIterable = tx.findLinks("Change", filesIterable, "file")
      val changesIterable =
        changesByCommitsIterable.intersect(changesByFileIterable)

      println("time: " + (System.currentTimeMillis - before))

      changesIterable.map(new DbChange(_)).toList
    }
  }

  implicit class ExtendedStoreTransaction(tx: StoreTransaction) {
    def findOption(entityType: String, property: String, value: String): Option[Entity] = {
      val iterable = tx.find(entityType, property, value)
      if (!iterable.isEmpty()) Some(iterable.getFirst) else None
    }
    def findIdOption(entityType: String, property: String, value: String): Option[EntityId] = {
      findOption(entityType, property, value).map(_.getId)
    }
  }
}
