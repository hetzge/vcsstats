package de.hetzge.svnview

import java.io.File

import org.apache.commons.io.FileUtils

import de.hetzge.svnview.model._
import de.hetzge.svnview.patcher.VcsFiles
import de.hetzge.svnview.db.Api

import jetbrains.exodus.entitystore.PersistentEntityStores

import cats._
import cats.effect._

import scala.concurrent._
import scala.concurrent.duration._

import vcs._
import monix.reactive.Consumer

object Main extends App {

  var lastCommitKey: String = null

  new File("target/example/journal/LOCK").delete()
  FileUtils.deleteDirectory(new File("data/test"))

  val api = new Api(PersistentEntityStores.newInstance(s"data/test"))

  // val vcs = new SvnVcs(SvnConfig(url = "http://svn.apache.org/repos/asf/mina/"))
  val vcs = new GitVcs(GitConfig(uri = "https://gitlab.com/hetzge/vcsstats.git"))

  import akka.actor._
  import de.hetzge.svnview.actor._
  import akka.pattern.ask
  val actorSystem  = ActorSystem("testSystem")
  val rootActorRef = actorSystem.actorOf(Props[RootActor])

  def process(change: Change): Unit = {
    // val changeReport = vcsFiles.apply(change)
    lastCommitKey = change.commitKey

    rootActorRef ! ChangeEvent(
      user = change.author,
      file = change.newFile,
      commit = change.commitKey,
      timestamp = change.instant.toEpochMilli,
      linesAdded = change.addedLineCount,
      linesDeleted = change.deletedLineCount
    )

    api.saveChange(change)
  }

  import monix.execution.Scheduler.Implicits.global
  import scala.util._

  import scala.concurrent.duration._
  import akka.util._
  implicit val timeout = Timeout(5 seconds)

  vcs
    .patches()
    .foreach(process)
    .andThen {
      case Success(_)     => api.print()
      case Failure(error) => error.printStackTrace
    }
    .andThen {
      case Success(_) =>
        for {
          files <- (rootActorRef ? GetFilesCommand)
          stat  <- (rootActorRef ? GetStatCommand(Filter()))
        } yield { 
          println("f: " + files) 
          println("s: " + stat)
        }
    }

  /*
  new Thread("poll") {
    override def run() = {
      while (true) {
        val newLastCommitKey = vcs.lastCommitKey
        val hasNewChanges = newLastCommitKey != lastCommitKey

        println(
          "poll! " + hasNewChanges + " " + newLastCommitKey + " " + lastCommitKey)

        if (hasNewChanges) {
          val patches = Await.result(
            vcs.patches(fromCommitKeyOption = Some(lastCommitKey)).runAsync,
            999.hours)
          patches.foreach(process)
          lastCommitKey = newLastCommitKey
        }

        Thread.sleep(5000)
      }
    }
  }.start()
   */
}
