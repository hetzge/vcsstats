package de.hetzge.svnview.actor

import de.hetzge.svnview.model._

import akka.actor._
import akka.util._
import akka.pattern._
import akka.persistence._
import akka.persistence.query._
import akka.persistence.query.journal.leveldb.scaladsl._
import akka.persistence.journal._
import akka.cluster.sharding._
import akka.stream._

import java.time._

import scala.util._
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

/**
  * Marker interface for persistent events
  */
sealed trait Persistent

final case class ChangeEvent(
    user: String,
    file: String,
    commit: String,
    timestamp: Long,
    linesAdded: Int,
    linesDeleted: Int
) extends Persistent {
  def instant = Instant.ofEpochMilli(timestamp)
  def apply(filter: Filter): Boolean = {
    val time     = instant
    val isTime   = time.equals(filter.from) || time.equals(filter.to) || (time.isAfter(filter.from) && time.isBefore(filter.to))
    val isUser   = (filter.includeUsers.isEmpty || filter.includeUsers.contains(user)) && !filter.excludeUsers.contains(user)
    val isCommit = (filter.includeCommits.isEmpty || filter.includeCommits.contains(commit)) && !filter.excludeCommits.contains(commit)
    val isFile   = (filter.includeFiles.isEmpty || filter.includeFiles.contains(file)) && !filter.excludeFiles.contains(file)

    isTime && isUser && isCommit && isFile
  }
}

final case object GetUsersCommand
final case object GetCommitsCommand
final case object GetFilesCommand

final case class GetStatCommand(filter: Filter)

final case class EntityEnvelop(entityId: String, message: Object)

object RootActor {
  val PERSISTENCE_ID = "ROOT"
}
final class RootActor extends PersistentActor {
  override def persistenceId = RootActor.PERSISTENCE_ID

  private val nodesActorRef = context.actorOf(Props[NodesActor])
  private val statClusterShardingActorRef = ClusterSharding(context.system).start(
    typeName = "Stat",
    entityProps = Props[StatActor],
    settings = ClusterShardingSettings(context.system),
    extractEntityId = {
      case EntityEnvelop(entityId, message) => (entityId, message)
    },
    extractShardId = {
      case _ => "0"
    }
  )

  override val receiveRecover: Receive = {
    case ce: ChangeEvent => on(ce)
  }

  override def receiveCommand = {
    case e: ChangeEvent        => persist(e)(on(_))
    case c: GetStatCommand     => on(c)
    case c @ GetUsersCommand   => nodesActorRef forward c
    case c @ GetCommitsCommand => nodesActorRef forward c
    case c @ GetFilesCommand   => nodesActorRef forward c
    case _                     => ()
  }

  private def on(changeEvent: ChangeEvent) = context.children.foreach(_ forward changeEvent)
  private def on(getStatCommand: GetStatCommand) = {
    val filter = getStatCommand.filter
    statClusterShardingActorRef forward EntityEnvelop(filter.key, StatActor.SetupCommand(filter))
    statClusterShardingActorRef forward EntityEnvelop(filter.key, getStatCommand)
  }
}

final class NodesActor extends Actor {
  private var users   = Set[String]()
  private var commits = Set[String]()
  private var files   = Set[String]()

  override def receive = {
    case c: ChangeEvent    => on(c)
    case GetUsersCommand   => sender ! users
    case GetCommitsCommand => sender ! commits
    case GetFilesCommand   => sender ! files
    case _                 => ()
  }

  private def on(changeEvent: ChangeEvent) = {
    users = users + changeEvent.user
    files = files + changeEvent.file
    commits = commits + changeEvent.commit
  }
}

object StatActor {
  final case class SetupCommand(filter: Filter)
}
final class StatActor extends Actor {
  private var filterOption: Option[Filter] = None
  private var stat                         = Stat()

  override def receive = {
    case ce: ChangeEvent if ce(filterOption.get) => on(ce)
    case GetStatCommand                          => {
      println("getStatCommand")
      sender ! stat
    }
    case c: StatActor.SetupCommand               => on(c)
    case _                                       => ()
  }

// TODO wait for init
  private def on(setupCommand: StatActor.SetupCommand) = {
    filterOption = Some(setupCommand.filter)
    PersistenceQuery(context.system)
      .readJournalFor[LeveldbReadJournal](LeveldbReadJournal.Identifier)
      .eventsByPersistenceId(RootActor.PERSISTENCE_ID)
      .runForeach(self ! _)(ActorMaterializer()(context.system))
  }

  private def on(changeEvent: ChangeEvent) = {
    stat = stat.copy(
      linesAdded = stat.linesAdded + changeEvent.linesAdded,
      linesDeleted = stat.linesDeleted + changeEvent.linesDeleted
    )
  }
}
