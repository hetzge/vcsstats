package de.hetzge.svnview.view

import akka.actor._
import akka.pattern.{ask, pipe}
import akka.http.scaladsl.model.ws.TextMessage

import de.hetzge.svnview.db._
import de.hetzge.svnview.model._
import de.hetzge.svnview.model.ui._

import java.util.UUID
import java.util.concurrent.ConcurrentHashMap

import io.circe._
import io.circe.syntax._
import io.circe.generic.extras._
import io.circe.generic.extras.auto._

import gnieh.diffson.circe._
import gnieh.diffson.HashedLcs
import gnieh.diffson.DynamicProgLcs

import scala.collection.mutable.ListBuffer
import scala.collection.mutable.HashMap
import scala.concurrent.Future
import scala.concurrent.Await
import scala.concurrent.duration._

import collection.JavaConverters._

import cats._
import cats.data._
import cats.implicits._
import cats.syntax._
import cats.syntax.semigroup._
import cats.instances.all._

import monix._
import monix.eval._
import monix.execution.Scheduler.Implicits.global
import monix.execution._

// https://twitter.com/alexelcu/status/1041735085857431553
import monix.execution.atomic._

final case class UiEvent(uiId: UiId, event: String, content: Json)
final case object Render
final case class ApplyJson(json: Json)

final case class UiId(value: String = UUID.randomUUID().toString) extends AnyVal

trait Ui {
  lazy val id = UiId()
  def on: PartialFunction[(String, Json), Task[Unit]] = {
    case _ => Task { println("ignore") }
  }
  def json: Task[Json]   = Task.now(Json.obj())
  def init(): Task[Unit] = Task.now(())
  def executeInit(implicit uiContext: UiContext) =
    init.runAsync.foreach(ignore => uiContext.render)
}

final class UiContext(val actor: ActorRef, val api: Api) {
  private val uis = new ConcurrentHashMap[UiId, Ui].asScala

  def registerUi(ui: Ui): Unit      = uis.put(ui.id, ui)
  def getUi(uiId: UiId): Option[Ui] = uis.get(uiId)
  def render(): Unit                = actor ! Render
}

object UiActor {
  def props(websocketActor: ActorRef)(implicit api: Api) =
    Props(new UiActor(websocketActor))
}
final class UiActor(val websocketActor: ActorRef)(implicit api: Api) extends Actor {

  private implicit val uiContext = new UiContext(self, api)

  private var lastJson: Json = Json.obj()

  private val rootUi = new RootUi()
  rootUi.executeInit

  def receive = {
    case UiEvent(id, event, content) =>
      uiContext.getUi(id) match {
        case Some(ui) =>
          ui.on((event, content)).runAsync.foreach { _ =>
            render()
          }
        case None => throw new IllegalStateException
      }
    case Render => render()
    case ApplyJson(json) =>
      val before = System.currentTimeMillis
      val diff   = new JsonDiff(new HashedLcs(new DynamicProgLcs()))
      val patch  = diff.diff(lastJson, json, false)
      println("time diff: " + (System.currentTimeMillis - before))

      lastJson = json

      websocketActor ! WebsocketResponseMessage(TextMessage(s"""{"action":"PATCH_MODEL","content":${patch.toString}}"""))

    case _ => render()
  }

  private def render(): Unit = {
    rootUi.json.runAsync.map(ApplyJson(_)).pipeTo(self)
  }
}

final class RootUi()(implicit context: UiContext) extends Ui {
  val tableUi = new StatTableUi()
  val treeUi  = new StatTreeRowUi(new StatTreeRow(name = "all"))

  override def init() = {
    for {
      _ <- Task.gatherUnordered((List(tableUi.init(), treeUi.init())))
    } yield (())
  }

  override def json = {
    for {
      tableJson <- tableUi.json
      treeJson  <- treeUi.json
    } yield {
      Json.obj(
        ("table", tableJson),
        ("tree", treeJson)
      )
    }
  }
}

final class StatTableUi()(implicit context: UiContext) extends Ui {

  private val tableAtomic = Atomic(StatTable(List(), List()))

  context.registerUi(this)

  override def init() = {
    for {
      table <- context.api.table()
      _     <- Task.now(tableAtomic.set(table))
      _     <- sort(0)
    } yield ()
  }

  def sort(columnIndex: Int) = Task {
    tableAtomic.transform { table =>
      table.sort(columnIndex)
    }
  }

  override def on = {
    case ("sort", content) => {
      val columnIndex = content.hcursor.get[Int]("columnIndex").getOrElse(0)
      sort(columnIndex)
    }
  }

  override def json = Task {
    val table = tableAtomic.get

    Json.obj(
      ("id", Json.fromString(id.value)),
      ("model", table.json)
    )
  }
}

final class StatTreeRowUi(_row: StatTreeRow = StatTreeRow())(implicit context: UiContext) extends Ui {

  val rowAtomic: Atomic[StatTreeRow]                      = Atomic(_row)
  val lastSortedColumnKeyAtomic: Atomic[String]           = Atomic("NAME")
  val childrenAtomic: Atomic[Option[List[StatTreeRowUi]]] = Atomic(None.asInstanceOf[Option[List[StatTreeRowUi]]])

  context.registerUi(this)

  override def init() = {
    for {
      statGroup <- context.api.query(row.filter)
    } yield {
      rowAtomic.transform(row => row.copy(statGroup = statGroup))
    }
  }

  override def on = {
    case ("toggle", _) => toggleChildren
    case ("sort", content) => {
      val columnKey = content.hcursor.get[String]("columnKey").getOrElse("NAME")
      sortChildren(columnKey)
    }
  }

  private def toggleChildren(): Task[Unit] = {
    for {
      newValue <- Task {
        (childrenAtomic.get match {
          case None            => Some(row.prepareChildren.map(new StatTreeRowUi(_)))
          case Some(_ :: tail) => None
          case _               => Some(childrenAtomic.get)
        }).asInstanceOf[Option[List[StatTreeRowUi]]]
      }
      _ <- Task { childrenAtomic.set(newValue) }
      // TODO batch
      _ <- Task.gather(newValue.getOrElse(List()).map(_.init))
      _ <- sortChildren()
    } yield ()
  }

  private def sortChildren(columnKey: String = lastSortedColumnKeyAtomic.get): Task[Unit] = {
    lastSortedColumnKeyAtomic.set(columnKey)
    childrenAtomic.transform(_ match {
      case Some(children) => {
        val ordering = columnKey match {
          case "NAME" => {
            Ordering.by { rowUi: StatTreeRowUi =>
              val row = rowUi.row
              row.groupType match {
                case CommitStatGroupType =>
                  row.statGroup.context.commits.head match {
                    case commit: Commit => commit.instant.getEpochSecond.toString
                    case _              => row.name
                  }
                case _ => row.name
              }
            }
          }
          case "ADDED" =>
            Ordering
              .by((_: StatTreeRowUi).row.statGroup.stat.linesAdded)
              .reverse
          case "REMOVED" =>
            Ordering
              .by((_: StatTreeRowUi).row.statGroup.stat.linesDeleted)
              .reverse
          case "FILE" =>
            Ordering
              .by((_: StatTreeRowUi).row.statGroup.context.files.size)
              .reverse
          case "USER" =>
            Ordering
              .by((_: StatTreeRowUi).row.statGroup.context.users.size)
              .reverse
          case "COMMIT" =>
            Ordering
              .by((_: StatTreeRowUi).row.statGroup.context.commits.size)
              .reverse
          case _ => Ordering.by[StatTreeRowUi, Int](_ => 0)
        }

        Some(children.sorted(ordering).sortBy(_.row.groupType.key))
      }
      case None => None
    })
    Task
      .gatherUnordered(childrenAtomic.get match {
        case Some(children) => children.map(_.sortChildren(columnKey))
        case None           => Nil
      })
      .map(_ => ())
  }

  private def row = rowAtomic.get

  override def json = {
    val children = childrenAtomic.get
    for {
      childrenJsons <- Task.gather(children.getOrElse(Nil).map(_.json))
    } yield {
      /*
       * None = not loaded
       * Nil = no children
       */
      val childrenJson =
        if (children.isDefined) Json.arr(childrenJsons: _*) else Json.Null

      Json.obj(
        ("id", Json.fromString(id.value)),
        ("model", rowAtomic.get.json),
        ("children", childrenJson)
      )
    }
  }
}
