package de.hetzge.svnview.view

import scala.io.StdIn
import scala.concurrent.Future

import akka.http.scaladsl.Http
import akka.actor.ActorSystem
import akka.http.scaladsl.model.HttpResponse
import akka.http.scaladsl.model.ws.UpgradeToWebSocket
import akka.http.scaladsl.model.HttpRequest
import akka.http.scaladsl.model.HttpMethods._
import akka.http.scaladsl.model.Uri
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Flow
import akka.http.scaladsl.model.ws.Message
import akka.http.scaladsl.model.ws.TextMessage
import akka.http.scaladsl.model.ws.BinaryMessage
import akka.stream.scaladsl.Source
import akka.stream.scaladsl.Sink
import akka.actor.ActorRef
import akka.stream.OverflowStrategy
import akka.stream.scaladsl.Keep
import akka.actor.Actor
import akka.actor.Props
import akka.NotUsed
import akka.stream.scaladsl.SourceQueue
import akka.stream.scaladsl.SourceQueueWithComplete
import akka.http.scaladsl.model.ws.TextMessage.Strict
import akka.http.scaladsl.server.directives.ContentTypeResolver

import org.reactivestreams.Publisher

import io.circe._
import io.circe.generic.auto._
import io.circe.parser._
import io.circe.syntax._
import io.circe.optics.JsonPath._

import jetbrains.exodus.entitystore.PersistentEntityStores

import de.hetzge.svnview.db._

import monix.reactive.Observable
import monix.reactive.OverflowStrategy.BackPressure
import monix.execution.Scheduler.Implicits.global

final case class WebsocketEventMessage(event: String, content: Json)
final case class WebsocketResponseMessage(message: Message)

object Server extends App {
  implicit val system       = ActorSystem()
  implicit val materializer = ActorMaterializer()

  val api = new Api(PersistentEntityStores.newInstance(s"data/test"))

  import akka.http.scaladsl.server.Directives._
  import ContentTypeResolver.Default
  val route =
    path("start") {
      getFromFile(s"web/index.html") // uses implicit ContentTypeResolver
    } ~
      path("socket") {
        get { ctx =>
          ctx.complete {
            ctx.request.header[UpgradeToWebSocket] match {
              case Some(upgrade) => {
                val (responseQueue, responsePublisher) =
                  Source
                    .queue[Message](bufferSize = 1000, OverflowStrategy.backpressure)
                    .toMat(Sink.asPublisher(false))(Keep.both)
                    .run
                val messageSink = Sink.actorRef[Message](system.actorOf(WebsocketActor.props(responseQueue)(api)), "")
                upgrade.handleMessagesWithSinkSource(inSink = messageSink, outSource = Source.fromPublisher(responsePublisher))
              }
              case None =>
                HttpResponse(status = 400, entity = "Not a valid websocket request!")
            }
          }
        }
      }

  object WebsocketActor {
    def props(queue: SourceQueue[Message])(implicit api: Api) = {
      Props(new WebsocketActor(queue))
    }
  }
  class WebsocketActor(queue: SourceQueue[Message])(implicit api: Api) extends Actor {
    private val uiActor = system.actorOf(UiActor.props(self)(api))

    def receive = {
      case WebsocketEventMessage(event, content) =>
        event match {
          case "HANDSHAKE" => uiActor ! Render
          case "CALLBACK" =>
            val cursor = content.hcursor
            for {
              id    <- cursor.get[String]("id")
              event <- cursor.get[String]("event")
              data  <- cursor.get[Json]("data")
            } yield {
              println(id + " " + event + " " + data)
              uiActor ! UiEvent(UiId(id), event, data)
            }
          case message => println("ignore: " + message)
        }
      case WebsocketResponseMessage(message) => queue.offer(message)
      case TextMessage.Strict(text)          => onTextMessage(text)
      case BinaryMessage.Strict(binary)      => ???
      case message                           => println("ignore: " + message)
    }

    /** Transforms a TextMessage to a WebsocketEvent and send it to itself. */
    private def onTextMessage(text: String): Unit = {
      parse(text) match {
        case Right(parsed) =>
          for {
            action  <- parsed.hcursor.get[String]("action")
            content <- parsed.hcursor.get[Json]("content")
          } yield {
            self ! WebsocketEventMessage(action, content)
          }
        case Left(error) => throw new IllegalStateException(error.underlying)
      }
    }
  }

  val bindingFuture =
    Http().bindAndHandle(handler = route, interface = "localhost", port = 8080)

  println(s"Server online at http://localhost:8080/\nPress RETURN to stop...")
  StdIn.readLine()

  bindingFuture
    .flatMap(_.unbind())
    .onComplete(_ => system.terminate())
}
