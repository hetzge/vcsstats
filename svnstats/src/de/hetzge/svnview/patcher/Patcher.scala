package de.hetzge.svnview.patcher

import scala.collection.mutable.HashMap

import org.scalatest.FlatSpec
import org.scalatest.Matchers

import com.zutubi.diff.PatchType
import com.zutubi.diff.unified.UnifiedHunk.LineType

import java.time.Instant
import de.hetzge.svnview.vcs._
import de.hetzge.svnview.model._
import scala.collection.mutable.ListBuffer

final case class ChangeReport(val change: Change, val removed: Seq[VcsFileLine], val added: Seq[VcsFileLine]) {
  def lines: Seq[VcsFileLine] = added ++ removed
}

final case class VcsFileLine(val lineNumber: Int, val content: String, val author: String) {
  def isEmpty               = content.isEmpty
  def characterCount        = content.length()
  def spaceCharacterCount   = content.toCharArray().count(_ == ' ')
  def noSpaceCharacterCount = characterCount - spaceCharacterCount
}

final case class VcsFile(private var _lines: List[VcsFileLine] = List()) {
  def lineCount              = _lines.size
  def isEmpty                = _lines.isEmpty
  def lines                  = _lines
  def line(lineNumber: Long) = _lines(lineNumber.toInt - 1)

  def apply(change: Change): ChangeReport = {

    try {

      if (change.patchType == PatchType.ADD) {
        if (!isEmpty) {
          throw new IllegalStateException("try to apply add change on non empty file")
        }

        _lines = change.hunks
          .flatMap(_.lines)
          .zipWithIndex
          .map {
            case (hunkLine, index) =>
              VcsFileLine(index + 1, hunkLine.content, change.author)
          }
          .toList

        return new ChangeReport(change, List(), _lines)
      } else {

        val removed = new ListBuffer[VcsFileLine]
        val added   = new ListBuffer[VcsFileLine]

        val hunksByOldLineNumber = change.hunks.groupBy(_.oldLineNumber)
        val removeLineNumbers =
          change.hunks.flatMap(_.deletedLineRange.toList).toSet
        _lines = (1L to lines.size).toList.flatMap { oldLineNumber =>
          if (hunksByOldLineNumber.contains(oldLineNumber)) {
            val addedLines = new ListBuffer[VcsFileLine]
            val hunks      = hunksByOldLineNumber.get(oldLineNumber).get
            for (hunk <- hunks) {
              var offset = 0
              for (hunkLine <- hunk.lines) {

                if (hunkLine.lineType == LineType.DELETED) {
                  removed += line(oldLineNumber + offset)
                  offset += 1
                } else if (hunkLine.lineType == LineType.COMMON) {
                  val newLineNumber = (oldLineNumber + added.size - removed.size).toInt
                  addedLines += line(oldLineNumber + offset).copy(lineNumber = newLineNumber)
                  offset += 1
                } else if (hunkLine.lineType == LineType.ADDED) {
                  val newLineNumber = (oldLineNumber + added.size - removed.size + 1).toInt
                  val addedLine     = VcsFileLine(newLineNumber, hunkLine.content, change.author)
                  added += addedLine
                  addedLines += addedLine
                } else {
                  throw new IllegalStateException(s"unknown ${classOf[LineType].getSimpleName} ${hunkLine.lineType}")
                }
              }
            }

            addedLines.toList
          } else if (removeLineNumbers.contains(oldLineNumber)) {
            List()
          } else {
            val newLineNumber = (oldLineNumber + added.size - removed.size).toInt
            List(line(oldLineNumber).copy(lineNumber = newLineNumber))
          }
        }.toList

        return ChangeReport(change, removed, added)
      }
    } catch {
      case e: Exception => {
        println(toString)
        println(change.hunks.map(_.debugString).mkString("\n"))

        throw new IllegalStateException(e)
      }
    }
  }

  override def toString: String =
    (1 to lineCount).toList
      .map(lineNumber => s"$lineNumber ${line(lineNumber).content}")
      .mkString("\n")
}

class VcsFiles(private val files: HashMap[String, VcsFile] = new HashMap) {

  def apply(change: Change): ChangeReport = {
    manage(change)
    return files
      .get(change.newFile)
      .map(_.apply(change))
      .getOrElse(ChangeReport(change, List(), List()))
  }

  def fileCount          = files.size
  def file(name: String) = files.get(name)

  private def manage(change: Change) = {
    change.patchType match {
      case PatchType.ADD => {
        files.put(change.newFile, VcsFile())
      }
      case PatchType.COPY => {
        files.put(change.newFile, files.get(change.oldFile).get.copy())
      }
      case PatchType.RENAME => {
        files.put(change.newFile, files.get(change.oldFile).get)
        files.remove(change.oldFile)
      }
      case PatchType.DELETE => {
        files.remove(change.oldFile)
      }
      case _ =>
    }
  }

  override def toString: String =
    files.map(entry => s"${entry._1}\n${entry._2}").mkString("\n")
}

class Spec extends FlatSpec with Matchers {

  "VcsFiles" should "add the file if patch is of type ADD" in {

    val files = new VcsFiles()
    files.apply(Change("", "NEW", List(), PatchType.ADD, "test", Instant.now(), "TestUser", "REVISION"))

    files.fileCount should be(1)
    files.file("NEW") shouldBe defined
  }

  it should "delete the file if patch is of type DELETE" in {

    val files = new VcsFiles(HashMap("NEW" -> VcsFile()))
    files.apply(Change("NEW", "", List(), PatchType.DELETE, "test", Instant.now(), "TestUser", "REVISION"))

    files.fileCount should be(0)
    files.file("NEW") should be(None)
  }

  it should "rename the file if patch is of type RENAME" in {

    val files = new VcsFiles(HashMap("OLD" -> VcsFile()))
    files.apply(Change("OLD", "NEW", List(), PatchType.RENAME, "test", Instant.now(), "TestUser", "REVISION"))

    files.fileCount should be(1)
    files.file("OLD") should be(None)
    files.file("NEW") shouldBe defined
  }

  it should "not create a new file instance if RENAME" in {

    val file  = VcsFile()
    val files = new VcsFiles(HashMap("OLD" -> file))
    files.apply(Change("OLD", "NEW", List(), PatchType.RENAME, "test", Instant.now(), "TestUser", "REVISION"))

    files.file("NEW").get should be theSameInstanceAs (file)
  }

  it should "copy the file if patch is of type COPY" in {

    val files = new VcsFiles(HashMap("OLD" -> VcsFile()))
    files.apply(Change("OLD", "NEW", List(), PatchType.COPY, "test", Instant.now(), "TestUser", "REVISION"))

    files.fileCount should be(2)
    files.file("OLD") shouldBe defined
    files.file("NEW") shouldBe defined
  }

  it should "create a new file instance if COPY" in {

    val files = new VcsFiles(HashMap("OLD" -> VcsFile()))
    files.apply(Change("OLD", "NEW", List(), PatchType.COPY, "test", Instant.now(), "TestUser", "REVISION"))

    files.file("OLD").get should not be theSameInstanceAs(files.file("NEW").get)
  }

  "VcsFile" should "add lines" in {

    val file = VcsFile()
    val report = file.apply(
      Change(
        "",
        "NEW",
        List(new Hunk(0, 1, List(HunkLine("One", LineType.ADDED), HunkLine("Two", LineType.ADDED), HunkLine("Three", LineType.ADDED)))),
        PatchType.ADD,
        "test",
        Instant.now(),
        "TestUser",
        "REVISION"
      ))

    file.lineCount should be(3)
    file.line(1).content should equal("One")
    file.line(2).content should equal("Two")
    file.line(3).content should equal("Three")

    report.added.size should be(3)
    report.removed.size should be(0)
  }

  it should "change lines" in {
    val file = VcsFile(List(VcsFileLine(1, "1", "test"), VcsFileLine(2, "2", "test"), VcsFileLine(3, "3", "test")))
    val report = file.apply(
      Change(
        "NEW",
        "NEW",
        List(new Hunk(
          1,
          1,
          List(
            HunkLine("1", LineType.DELETED),
            HunkLine("2", LineType.DELETED),
            HunkLine("3", LineType.DELETED),
            HunkLine("One", LineType.ADDED),
            HunkLine("Two", LineType.ADDED)
          )
        )),
        PatchType.EDIT,
        "test",
        Instant.now(),
        "TestUser",
        "REVISION"
      ))

    file.lineCount should be(2)
    file.line(1).content should equal("One")
    file.line(2).content should equal("Two")

    report.added.size should be(2)
    report.removed.size should be(3)
  }

  it should "remove lines" in {
    val file = VcsFile(List(VcsFileLine(1, "1", "test"), VcsFileLine(2, "2", "test"), VcsFileLine(3, "3", "test")))
    val report = file.apply(
      Change(
        "NEW",
        "NEW",
        List(new Hunk(1, 1, List(HunkLine("1", LineType.DELETED), HunkLine("2", LineType.DELETED), HunkLine("3", LineType.DELETED)))),
        PatchType.EDIT,
        "test",
        Instant.now(),
        "TestUser",
        "REVISION"
      ))

    file.lineCount should be(0)

    report.added.size should be(0)
    report.removed.size should be(3)
  }

  it should "create correct line numbers at new file" in {

    val file = VcsFile()
    val report = file.apply(
      Change(
        "NEW",
        "NEW",
        List(new Hunk(1, 1, List(HunkLine("1", LineType.ADDED), HunkLine("2", LineType.ADDED), HunkLine("3", LineType.ADDED)))),
        PatchType.ADD,
        "test",
        Instant.now(),
        "TestUser",
        "REVISION"
      ))

    report.added(0).lineNumber should be(1)
    report.added(1).lineNumber should be(2)
    report.added(2).lineNumber should be(3)
  }

  it should "create correct line numbers at existing file" in {

    val file = VcsFile(List(VcsFileLine(1, "1", "test"), VcsFileLine(2, "2", "test"), VcsFileLine(3, "3", "test")))
    val report = file.apply(
      Change(
        "NEW",
        "NEW",
        List(new Hunk(2, 2, List(HunkLine("2", LineType.DELETED), HunkLine("1", LineType.ADDED), HunkLine("2", LineType.ADDED), HunkLine("3", LineType.ADDED)))),
        PatchType.EDIT,
        "test",
        Instant.now(),
        "TestUser",
        "REVISION"
      ))

    report.added(0).lineNumber should be(2)
    report.added(1).lineNumber should be(3)
    report.added(2).lineNumber should be(4)

    file.line(1).lineNumber should be(1)
    file.line(2).lineNumber should be(2)
    file.line(3).lineNumber should be(3)
    file.line(4).lineNumber should be(4)
    file.line(5).lineNumber should be(5)
  }
}
