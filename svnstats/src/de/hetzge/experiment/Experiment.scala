package de.hetzge.experiment

import akka._
import akka.actor._
import akka.dispatch._
import akka.persistence._
import akka.persistence.query._
import akka.persistence.query.journal.leveldb.scaladsl._
import akka.persistence.journal._
import akka.stream._
import akka.stream.scaladsl._

import scala.concurrent._
import scala.concurrent.duration._

import java.io.File

trait Event
trait PersistentEvent extends Event {
  def tags: Set[String] = Set.empty
}
trait Command
trait State[T] {
  final def apply(event: Event): T = update(event)
  def update: PartialFunction[Event, T]
}

final case class MyEvent(value: Int) extends PersistentEvent {
  override def tags = Set(if (value % 2 == 0) "EVEN" else "ODD")
}

final case class MyCommand(value: Int) extends Command
final case object PrintCommand         extends Command

class MyTaggingEventAdapter extends WriteEventAdapter {
  override def toJournal(event: Any) = event match {
    case p: PersistentEvent => Tagged(event, p.tags)
    case _                  => event
  }
  override def manifest(event: Any): String = ""
}

object Experiment extends App {
  new File("target/example/journal/LOCK").delete()

  private implicit val system       = ActorSystem("testSystem")
  private implicit val materializer = ActorMaterializer()(system)
  private val actorRef              = system.actorOf(Props[MyPersistentActor], "test")

  println("Hello Experiment")

  actorRef ! MyCommand(124)
  actorRef ! MyCommand(435)
  actorRef ! PrintCommand

  val queries = PersistenceQuery(system)
    .readJournalFor[LeveldbReadJournal](LeveldbReadJournal.Identifier)

  queries
    .eventsByTag("EVEN")
    .map(_.event)
    .runForeach(v => println("even: " + v))
  queries.eventsByTag("ODD").map(_.event).runForeach(v => println("odd: " + v))

  while (true) {
    actorRef ! MyCommand(535)
    Thread.sleep(10000)
  }
}

case class MyState(value: Int) extends State[MyState] {
  override def update = {
    case MyEvent(v) => copy(value + v)
  }
}

// TODO abstract
class MyPersistentActor extends PersistentActor {
  override def persistenceId = "sample-id-1"

  println("create actor")

  private var state: MyState = MyState(0)

  override val receiveRecover: Receive = {
    case event: Event                           => state = state(event)
    case SnapshotOffer(metadata, snapshot: Int) => state = MyState(snapshot)
  }

  override val receiveCommand: Receive = {
    case MyCommand(value) =>
      persist(MyEvent(value)) { event =>
        state = state(event)
        // context.system.eventStream.publish(value)
        saveSnapshot(state)
      }
    case PrintCommand => println(state)
    case "PRINT"      => println(state)
  }
}
