import mill._, scalalib._, scalafmt._

interp.repositories() = interp.repositories() ++ Seq(coursier.MavenRepository("https://jitpack.io"))
@
import $ivy.`com.github.yyadavalli::mill-ensime:0.0.2`

trait svnstats extends ScalaModule with ScalafmtModule {
  def scalaVersion = "2.12.4"
  def millSourcePath = ammonite.ops.pwd / "svnstats"
  def ivyDeps = Agg(
    ivy"com.lihaoyi::upickle:0.5.1",
    ivy"commons-io:commons-io:2.6",
    ivy"org.tmatesoft.svnkit:svnkit:1.9.0",
    ivy"org.eclipse.jgit:org.eclipse.jgit:4.9.0.201710071750-r",
    ivy"org.json:json:20171018",
    ivy"org.scalatest:scalatest_2.12:3.0.4",
    ivy"org.jetbrains.xodus:xodus-openAPI:1.2.3",
    ivy"org.jetbrains.xodus:xodus-entity-store:1.2.3",
    ivy"org.typelevel:cats-core_2.12:1.4.0",
    ivy"org.typelevel:cats-effect_2.12:1.0.0",
    ivy"io.monix:monix_2.12:3.0.0-8084549",
    ivy"com.typesafe.akka:akka-actor_2.12:2.5.16",
    ivy"com.typesafe.akka:akka-stream_2.12:2.5.16",
    ivy"com.typesafe.akka:akka-http_2.12:10.1.5",
    ivy"com.typesafe.akka:akka-persistence_2.12:2.5.16",
    ivy"com.typesafe.akka:akka-persistence-query_2.12:2.5.16",
    ivy"com.typesafe.akka:akka-cluster-sharding_2.12:2.5.16",
    ivy"org.iq80.leveldb:leveldb:0.7",
    ivy"org.fusesource.leveldbjni:leveldbjni-all:1.8",
    ivy"org.gnieh:diffson-circe_2.12:3.0.0",
    ivy"io.circe:circe-generic_2.12:0.9.3",
    ivy"io.circe:circe-optics_2.12:0.9.3",
    ivy"io.circe:circe-shapes_2.12:0.9.3",
    ivy"io.circe:circe-generic-extras_2.12:0.9.3",
    ivy"org.scala-stm:scala-stm_2.12:0.8",
    ivy"net.openhft:chronicle-wire:1.16.16",
    ivy"org.slf4j:slf4j-simple:1.7.25"
  )
  def unmanagedClasspath = T {
    Agg.from(ammonite.ops.ls(super.millSourcePath / ammonite.ops.up / "lib").map(PathRef(_)))
  }
}

object vcs extends svnstats {
  def mainClass = Some("de.hetzge.svnview.Main")
}

object ui extends svnstats {
  def mainClass = Some("de.hetzge.svnview.view.Server")
}

// object experiment extends svnstats {
//   def mainClass = Some("de.hetzge.experiment.Experiment")
// }

